import {Fragment, FunctionComponent, useEffect, useState} from 'react';
import Footer from './footer.component';
import NavBar from './NavBar.component';







const Layout: FunctionComponent = ({children})=>{




    return(
        <div>

         <Fragment>

                     <div className="content">
                       <NavBar/>
                       {children}
                       <Footer/>
                            </div>

             </Fragment>

        </div>
       

    );
};
export default Layout;