import { useRouter } from "next/dist/client/router"
import { Manga } from "../../../dto/mangas.model";



type IMangaDetailProps = {
    manga:Manga;
}
export default function MangaDetail(props:IMangaDetailProps){
    const rooter =useRouter();
    return(
        <main>
          {/* {  console.log("manga",props.manga)} */}
            <div>{props.manga.title}</div>
            <div>{props.manga.cover}</div>
            {props.manga.author&&<div>{props.manga.author.firstName} {props.manga.author.lastName}</div>}
            <div>{props.manga.year}</div>

        </main>
    )
}

export async function getServerSideProps(context: any) {
    const res = await fetch(`http://localhost:3001/mangas/${context.query.id}`)
    const manga : Manga = await res.json()
    if (!manga) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { manga }, // will be passed to the page component as props
    }
 }
 